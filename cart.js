$(function() {


LineItem = Backbone.Model.extend({
  
});

LineItemView = Backbone.View.extend({
  model:LineItem,
  tagName: "tr",
  template: _.template("<td><%= sku %></td><td><input name='li_qty' class='li_qty' value='<%= qty %>'></td><td><%= unit_price %></td><td><%= line_item_type %></td><td>weight: <%= weight %> dimensions: <%= dim_length %> x <%= dim_width %> x <%= dim_height %></td><td align='right'><%= qty * unit_price %></td>"),
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  },
  initialize: function() {
    this.model.bind('change', this.render, this);
    this.model.bind('destroy', this.remove,this);
    
  },
  events: {
    'change':'detectChange'
  },
  detectChange:function(){
    this.model.set({qty: this.$('.li_qty').val()});
    if (this.model.get('qty') === "0") {
      this.model.destroy();
    }
    
  }
});

LineItems = Backbone.Collection.extend({});

AppView = Backbone.View.extend({
  el: 'body',
  
  initialize: function(){
    this.lineItems = new LineItems;
    this.lineItems.bind('add', this.renderNewSku, this);
    this.lineItems.bind('add remove', this.recalcShippingOptions, this);
  },
  events: {
    'click #add_to_cart': 'createLineItemModel'
  },
  createLineItemModel: function(){
    this.lineItems.add({sku: $('#sku').val(), qty:$('#qty').val(), unit_price: $('#unit_price').val(), line_item_type:$('#line_item_type').val(), weight:$('#weight').val(), dim_length:$('#dim_length').val(),dim_width:$('#dim_width').val(),dim_height:$('#dim_height').val() });
  },
  renderNewSku: function(skuDetails) {
    var view = new LineItemView({model: skuDetails});
    $('#cart table').append(view.render().$el);
  },
  recalcShippingOptions: function(){
    console.log('recalc shipping options based on ' + this.lineItems.length + " line items");
  }
});


app = new AppView();
});


